package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type Cities struct {
	Id         int
	Name       string
	Population int
}

func main() {

	db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/db_golang")
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}
	res, err := db.Query("SELECT * FROM cities")
	defer res.Close()
	if err != nil {
		log.Fatal(err)
	}
	for res.Next() {
		var city Cities
		err := res.Scan(&city.Id, &city.Name, &city.Population)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v\n", city)
	}
}
